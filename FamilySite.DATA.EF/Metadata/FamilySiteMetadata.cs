﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilySite.DATA.EF//.Metadata
{
    #region Course
    public class CourseMetadata
    {
        //public int CourseID { get; set; }

        [Required(ErrorMessage = "required, must input data")]
        [StringLength(200, ErrorMessage = "Max 200 characters.")]
        [Display(Name = "Course")]
        public string CourseName { get; set; }//200

        [StringLength(500, ErrorMessage = "Max 200 characters.")]
        [Display(Name = "Description")]
        [UIHint("MultilineText")]
        public string Description { get; set; }//allow n, 500

        [Required(ErrorMessage = "required, must check box")]
        [Display(Name = "Active Course?")]
        public bool IsActive { get; set; }//bit
    }

    [MetadataType(typeof(CourseMetadata))]
    public partial class Course { }

    #endregion

    #region CourseCompletion
    public class CourseCompletionMetadata
    {
        // public int CourseCompletionID { get; set; }
        // public string UserID { get; set; }
        // public int CourseID { get; set; }

        [Required(ErrorMessage = "Completion date is required.")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        [Display(Name = "Date Completed")]
        public System.DateTime DateCompleted { get; set; }

    }

    [MetadataType(typeof(CourseCompletionMetadata))]
    public partial class CourseCompletion { }
    #endregion

    #region Lesson
    public class LessonMetadata
    {
        //public int LessonID { get; set; }
        //public int CourseID { get; set; }

        [Required(ErrorMessage = "Required, must input data")]
        [StringLength(200, ErrorMessage = "Max 200 characters")]
        [Display(Name = "Lesson")]
        public string LessonTitle { get; set; }//200

        [StringLength(300, ErrorMessage = "Max 300 characters")]
        [DisplayFormat(NullDisplayText = "[-N/A-]")]
        public string Introduction { get; set; }//nullable,300

        [StringLength(250, ErrorMessage = "Max 250 characters")]
        [DisplayFormat(NullDisplayText = "[-N/A-]")]
        [Display(Name = "URL of Video")]
        public string VideoURL { get; set; }//nullable,250

        [StringLength(100, ErrorMessage = "Max 100 characters")]
        [DisplayFormat(NullDisplayText = "[-N/A-]")]
        [Display(Name = "PDF File")]
        public string PdfFileName { get; set; }//nullable,100

        [Required(ErrorMessage = "required, must check box")]
        [Display(Name = "Active Lesson?")]
        public bool IsActive { get; set; }

        [Display(Name = "Reservation Limit")]
        [DisplayFormat(NullDisplayText = "[-N/A-]")]
        public Nullable<int> ReservationLimit { get; set; }

    }

    [MetadataType(typeof(LessonMetadata))]
    public partial class Lesson { }

    #endregion

    #region LessonView
    public class LessonViewMetadata
    {
        //public int LessonViewID { get; set; }
        //public string UserID { get; set; }
        //public int LessonID { get; set; }

        [Required(ErrorMessage = "View date is required")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        [Display(Name = "Date Viewed")]
        public System.DateTime DateViewed { get; set; }
    }

    [MetadataType(typeof(LessonViewMetadata))]
    public partial class LessonView { }
    #endregion

    #region Reservation
    public class ReservationMetadata
    {
        //public int ReservationID { get; set; }
        //public int LessonID { get; set; }
        //public string UserID { get; set; }

        [Required(ErrorMessage = "Reservation date is required.")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        [Display(Name = "Reservation Date")]
        public System.DateTime ReservationDate { get; set; }

    }

    [MetadataType(typeof(ReservationMetadata))]
    public partial class Reservation { }

    #endregion

    #region UserDetail
    public class UserDetailMetaData
    {
        //public string UserID { get; set; }

        [Required(ErrorMessage ="required, must input data")]
        [StringLength(50, ErrorMessage ="Max 50 characters")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }//50

        [Required(ErrorMessage = "required, must input data")]
        [StringLength(50, ErrorMessage = "Max 50 characters")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }//50

        [StringLength(50, ErrorMessage = "Max 150 characters")]
        [DisplayFormat(NullDisplayText = "[-N/A-]")]
        [Display(Name = "Avatar Image")]
        public string AvatarImage { get; set; }//nullable, 150

    }
    [MetadataType(typeof(UserDetailMetaData))]
    public partial class UserDetail
    {
        //This is a readonly field for display only
        [Display(Name ="Full Name")]
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
    }
    
    #endregion
}//namespace

