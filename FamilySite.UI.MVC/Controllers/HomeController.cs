﻿using System.Net;
using System.Net.Mail;
using System.Web.Mvc;


namespace FamilySite.UI.MVC.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        [HttpGet]
        public ActionResult Contact()
        {
            //ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(Models.ContactViewModel contactInfo)
        {
            if (!ModelState.IsValid)
            {
                return View(contactInfo);
            }

            string body = string.Format(
                $"Name: {contactInfo.Name}<br />"
                + $"Email: {contactInfo.Email}<br />"
                + $"Subject: {contactInfo.Subject}<br />"
                + $"Message: {contactInfo.Message}");

            MailMessage msg = new MailMessage(
                "no-reply@hjkangweb.com",
                "speedkhj@gmail.com",
                contactInfo.Subject,
                body);

            msg.IsBodyHtml = true;
            msg.CC.Add("postmaster@hjkangweb.com");
            msg.Priority = MailPriority.High;

            SmtpClient client = new SmtpClient("mail.hjkangweb.com");
            client.Credentials = new NetworkCredential("no-reply@hjkangweb.com", "kang1226@@");
            client.EnableSsl = false;
            client.Port = 25;

            using (client)
            {
                try
                {
                    client.Send(msg);
                }
                catch
                {
                    ViewBag.ErrorMessage = "There was an error sending your message.\n" + "Please try again";
                    return View();
                }
            }

            return View("ContactConfirmation", contactInfo);

        }
    }
}
